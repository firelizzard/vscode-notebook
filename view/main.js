window.addEventListener('message', event => {
    const { command, ...data } = event.data;

    switch (command) {
        case 'load': load(data.content); return

        case 'executed':
            const { id, result, error } = data
            const cell = cells[id]
            const { result: rel } = cell[0].nb

            if (error) {
                rel.addClass('error')
                if (error === void 0)
                    rel.text('Unknown error')
                else if (typeof error == 'object' && 'message' in error && 'stack' in error && 'type' in error)
                    rel.text(`${error.type}: ${error.message}`)
                else
                    rel.text(JSON.stringify(error))
            } else {
                rel.removeClass('error')
                if (result === void 0)
                    rel.html('')
                else
                    rel.text(result)
            }

            return
    }
})

const vscode = acquireVsCodeApi()
const selection = window.getSelection()
const state = {
    window: void 0,
    asdfqwer: 1
}

for (const key of Object.keys(window))
    state[key] = void 0

vscode.postMessage({ command: 'ready' })

function load(text) {
    let cellText = null, cellType = null
    for (const line of text.split(/[\n\r]+/)) {
        if (line.startsWith('```'))
            if (cellType == null) {
                cellText = ''
                cellType = line.substring(3)
            }
            else {
                prepareCell(cellText, cellType).appendTo(document.body)
                cellText = null
                cellType = null
            }
        else if (cellType == null)
            prepareLine(line).appendTo(document.body)
        else
            cellText += line
    }

    if (cellType != null)
        prepareCell(cellText, cellType).appendTo(document.body)
}

let cellId = 0
const cells = {}
function prepareCell(text, type) {
    const id = cellId++
    const cell = $('<cell>').attr({ type })
    const code = $('<code>').appendTo(cell).attr({ contenteditable: true }).text(text)
    const result = $('<result>').appendTo(cell)

    cell[0].nb = { code, result, id }
    cells[id] = cell

    code.keydown(e => {
        if (e.which != 13)
            return

        e.preventDefault()
    })

    code.keyup(e => {
        if (e.which != 13)
            return

        if (e.shiftKey) {
            vscode.postMessage({ command: 'execute', id, code: code.text() })
            return
        }

        let { startOffset: offset } = selection.getRangeAt(0)
        let text = code.text()

        if (offset == text.length) {
            if (!text.endsWith('\n'))
                text += '\n'
            code.text(text + '\n')
            offset = text.length
        } else {
            code.text(text.substring(0, offset) + '\n' + text.substring(offset))
            offset++
        }

        const range = document.createRange()
        range.setStart(code.contents()[0], offset)
        range.setEnd(code.contents()[0], offset)
        selection.removeAllRanges()
        selection.addRange(range)
    })

    return cell
}

function prepareLine(text) {
    const line = $('<line>')
    const content = $('<content>').appendTo(line).attr({ contenteditable: true }).text(text)
    const code = $('<code>').appendTo(line).attr({ contenteditable: true }).text(text)

    line[0].nb = { content, code }

    content.click(() => {
        const { startOffset: start, endOffset: end } = selection.getRangeAt(0)
        select(line, start, end)
    })

    code.blur(() => line.removeClass('raw'))

    let keydownStart;
    code.keydown(e => {
        keydownStart = selection.getRangeAt(0).startOffset

        switch (e.which) {
            case 13:
                e.preventDefault()
                return

            case 38:
                if (line.is(':first-child'))
                    return
                e.preventDefault()
                return

            case 40:
                if (line.is(':last-child'))
                    return
                e.preventDefault()
                return
        }
    })

    code.keyup(e => {
        const { startOffset: start, startContainer: text } = selection.getRangeAt(0)
        const { code } = line[0].nb

        switch (e.which) {
            case 8: // backspace
                if (keydownStart > 0 || line.is(':first-child'))
                    return

                merge(line.prev(), line)
                return

            case 46: // delete
                if (start < text.nodeValue.length || line.is(':last-child'))
                    return

                merge(line, line.next())
                return

            case 13: // return
                if (code.text().startsWith('```')) {
                    const cell = prepareCell('', code.text().substring(3))
                    line.replaceWith(cell)
                    cell[0].nb.code.focus()
                } else
                    split(line, start)
                return

            case 37:
                if (keydownStart > 0 || line.is(':first-child'))
                    return

                select(line.prev(), -1)
                return

            case 38:
                if (line.is(':first-child'))
                    return

                select(line.prev(), start)
                return

            case 39:
                if (start < text.nodeValue.length || line.is(':last-child'))
                    return

                select(line.next(), 0)
                return

            case 40:
                if (line.is(':last-child'))
                    return

                select(line.next(), start)
                return
        }
    })

    code.keyup(() => updateLine(line))
    updateLine(line)

    return line
}

function select(line, start, end) {
    const { code } = line[0].nb

    line.addClass('raw')
    code.focus()

    const text = code.contents().filter((_, x) => x.nodeType == 3)[0]
    const range = document.createRange()


    const { length } = text.nodeValue
    if (start < 0)
        start = length + start + 1
    else if (start > length)
        start = end = length

    if (typeof end !== 'number')
        end = start
    else if (end < 0)
        end = length + end + 1
    else if (end > length)
        end = length

    range.setStart(text, start)
    range.setEnd(text, end)

    selection.removeAllRanges()
    selection.addRange(range)
}

function split(line, offset) {
    const { code } = line[0].nb
    const text = code.contents().filter((_, x) => x.nodeType == 3)[0]

    const { length } = text.nodeValue
    if (offset < 0)
        offset = length + offset + 1
    else if (offset > length)
        offset = end = length

    const other = text.splitText(offset)
    $(other).remove()

    const next = prepareLine(other.nodeValue).insertAfter(line)
    next.addClass('raw')
    next[0].nb.code.focus()
}

function merge(first, second) {
    const { code: firstCode, content: firstContent } = first[0].nb
    const firstText = firstCode.text()

    const text = firstText + second[0].nb.code.text()
    firstCode.text(text)
    firstContent.text(text)
    second.remove()
    select(first, firstText.length)
}

const formats = {
    'header': /^(#+) (.*)$/,
    'bullet': /^  [+*-]/,
}

function updateLine(line) {
    const { code, content } = line[0].nb
    const text = code.text()
    const format = getFormat()

    // TODO replace with markdown parser
    if (format == 'header') {
        const [, level, value] = text.match(formats.header)
        line.attr('format', 'header ' + level.length)
        content.text(value)
    } else {
        line.attr('format', format || 'paragraph')
        content.text(code.text())
    }

    function getFormat() {
        for (const format in formats) {
            if (formats[format].test(text))
                return format
        }
    }
}