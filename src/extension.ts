import * as vm from 'vm';
import {
    commands, window,
    ExtensionContext,
    ViewColumn, Uri
} from 'vscode';

// this method is called when your extension is activated
export function activate(context: ExtensionContext) {
    console.log('Congratulations, your extension "vscode-notebook" is now active!')

    // The command has been defined in the package.json file
    // Now provide the implementation of the command with  registerCommand
    // The commandId parameter must match the command field in package.json
    context.subscriptions.push(commands.registerCommand('extension.sayHello', () => {
        // The code you place here will be executed every time your command is executed

        // Display a message box to the user
        window.showInformationMessage('Hello World!')
    }))

    context.subscriptions.push(commands.registerCommand('notebook.createNew', () => {
        const panel = window.createWebviewPanel('notebook', 'Untitled', ViewColumn.Active, {
            enableScripts: true,
            enableFindWidget: true,
            retainContextWhenHidden: false,
            // localResourceRoots: [
            //     Uri.file(path.join(context.extensionPath, 'view')),
            // ]
        })

        const sandbox = vm.createContext()

        const content = 'Hello World!\n```\n// write some code!\n```'
        const base = Uri.file(context.extensionPath).with({ scheme: 'vscode-resource' })
        // panel.webview.html = `<script>location = '${indexPath}'</script>`

        panel.webview.onDidReceiveMessage(({ command, ...data }) => {
            switch (command) {
                case 'ready':
                    commands.executeCommand('workbench.action.webview.openDeveloperTools')
                    panel.webview.postMessage({ command: 'load', content })
                    return

                case 'execute':
                    const { id, code } = data
                    try {
                        const result = vm.runInContext(code.trim(), sandbox)
                        if (result === void 0)
                            panel.webview.postMessage({ command: 'executed', id })
                        else
                            panel.webview.postMessage({ command: 'executed', id, result: JSON.stringify(result) || `${result}` })
                    } catch (error) {
                        if (typeof error == 'object' && 'message' in error && 'stack' in error && error.constructor.name.endsWith('Error')) {
                            const { message, stack } = error
                            error = { message, stack, type: error.constructor.name }
                        }
                        panel.webview.postMessage({ command: 'executed', id, error })
                    }
                    return
            }
        })

        panel.webview.html = `
        <!DOCTYPE html>
        <html>
            <head>
                <link rel="stylesheet" href="${base}/view/main.css" />
                <script defer src="${base}/node_modules/jquery/dist/jquery.slim.min.js"></script>
                <script defer src="${base}/view/main.js"></script>
            </head>
            <body></body>
        </html>`
    }))
}

// this method is called when your extension is deactivated
export function deactivate() {
}